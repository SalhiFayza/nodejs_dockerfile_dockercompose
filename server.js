
const express = require('express')
const app = express()
const b = express.urlencoded({extended:true})


app.get('/',(req,res,next)=>{
    res.send('Welcome Salhi Fayza DevOps 2022 2023')
})

var Students = [
    {id:0,firstName:'Fayza1',lastName:'Salhi1',Speciality:'DevOps'},
    {id:1,firstName:'Fayza2',lastName:'Salhi2',Speciality:'Full-Stack'},
    {id:2,firstName:'Fayza3',lastName:'Salhi3',Speciality:'Tester'},
    {id:3,firstName:'Fayza4',lastName:'Salhi4',Speciality:'Programmer'},
    {id:4,firstName:'Fayza5',lastName:'Salhi5',Speciality:'Mobile Apps'},
    {id:4,firstName:'Fayza5',lastName:'Salhi5',Speciality:'Mobile Apps'},
    {id:4,firstName:'Fayza5',lastName:'Salhi5',Speciality:'Mobile Apps'},
]
app.get('/students',(req,res,next)=>{
    res.send(Students)
})

app.get('/student/:id',(req,res,next)=>{
    let newid = req.params.id
    let newStudent = Students.filter(student=>student.id==newid)
    if(newStudent)
    {
    res.send(newStudent)
}else{
    res.send("We don't have this student")
}

})
app.delete('/student/delete/:id',(req,res,next)=>{
 let Student = Students.find(student=>student.id==req.params.id)
 let postionStudent = Students.indexOf(Student)
 Students.splice(postionStudent,1)
 res.send(Students)
})

app.post('/student/addStudent',b,(req,res,next)=>{
    let newStudent ={
        id:req.body.id,
        firstName:req.body.firsName,
        lastName:req.body.lastName,
        Speciality:req.body.Speciality
    } 
    Students.push(newStudent)
   res.send(Students)
   })
   app.put('/student/put/:id',b,(req,res,next)=>{
    let Student = Students.find(student=>student.id==req.params.id)
    Student.firstName=req.body.firstName
    Student.lastName=req.body.lastName
    Student.Speciality=req.body.Speciality
   res.send(Student)
   })
   app.patch('/student/patch/:id',b,(req,res,next)=>{
    let Student = Students.find(student=>student.id==req.params.id)
    Student.firstName=req.body.firstName
    Student.lastName=req.body.lastName
    Student.Speciality=req.body.Speciality
   res.send(Student)
   })
   app.post('/student/post/:id',b,(req,res,next)=>{
    let Student = Students.find(student=>student.id==req.params.id)
    Student.firstName=req.body.firstName
    Student.lastName=req.body.lastName
    Student.Speciality=req.body.Speciality
   res.send(Student)
   })

app.listen(3080,()=>console.log("server running on port 3080"))