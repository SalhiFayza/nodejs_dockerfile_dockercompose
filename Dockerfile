FROM node:latest

WORKDIR /usr/src/app/nodejsproject

COPY package*.json ./

RUN npm install

EXPOSE 3080

CMD ["npm", "run", "dev"]
